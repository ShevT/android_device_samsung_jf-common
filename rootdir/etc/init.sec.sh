#!/system/bin/sh

# Create an empty DB file nwk_info.db to fix error:
#    qcril_data_fd_readFdPolicyFromDB: nwk_info DB open error

NWKDB="/data/user_de/0/com.android.providers.telephony/databases/nwk_info.db"
if [ ! -f $NWKDB ]; then
    touch $NWKDB
    chown radio:radio $NWKDB
    chmod 0660 $NWKDB
fi
